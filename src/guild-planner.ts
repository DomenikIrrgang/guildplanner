import { CharacterDataSheet } from "./data/character-data-sheet";
import { CharacterDataSource } from "./data/character-data-source";
import { Character } from "./data/character";
import { RealmDataSheet } from "./data/realm-data-sheet";
import { Realm } from "./data/realm";
import { Spreadsheet } from "./data/spreadsheet";
import { Config } from "./config";

export class GuildPlanner {

    public characterData: CharacterDataSource = new CharacterDataSheet(new Spreadsheet())
    public realmData = new RealmDataSheet(Config.sheets.realms, new Spreadsheet())

    public getCharacters(): Character[] {
        return this.characterData.getAllCharacters()
    }

    public getRealms(): Realm[] {
        return this.realmData.getAllRealms()
    }
}