import { GuildPlanner } from "./guild-planner";
import { Character } from "./data/character";
import { Realm } from "./data/realm";

let guildPlanner = new GuildPlanner()

function getCharacters(): string {
    return guildPlanner.getCharacters()[0].clazz.toString()
}

function getRealms(): string {
    return guildPlanner.getRealms()[0].name
}

function getGuildPlanner(): GuildPlanner {
    return new GuildPlanner()
}

function onOpen() {
    var ui = SpreadsheetApp.getUi();
    // Or DocumentApp or FormApp.
    ui.createMenu('Characters')
        .addItem('Add Character', 'hello')
        .addItem('Init Realms', 'initRealmData')
        .addItem('Add Realm', 'addRealmDialog')
        .addItem('Test Add Realm', 'testAddRealm')
        .addSeparator()
        .addSubMenu(ui.createMenu('Sub-menu')
            .addItem('Second item', 'menuItem2'))
        .addToUi();
}

function initRealmData() {
    guildPlanner.realmData.init()
}

function hello() {
    let ui = SpreadsheetApp.getUi();
    let html = HtmlService.createTemplateFromFile("src/dialogs/add-character.html").evaluate();
    ui.showModalDialog(html, "Add Character");
}

function addRealmDialog() {
    let ui = SpreadsheetApp.getUi();
    let html = HtmlService.createTemplateFromFile("src/dialogs/add-realm.html").evaluate().setHeight(90);
    ui.showModalDialog(html, "Add Realm");
}

function addRealm(realm: Realm) {
    getGuildPlanner().realmData.save(realm);
}

function testAddRealm() {
    addRealm({ name: "Beta" })
}