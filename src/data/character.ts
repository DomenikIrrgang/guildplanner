import { Race } from "./race";
import { Clazz } from "./class";
import { Realm } from "./realm";

export class Character {

    constructor(
        public id: number,
        public realmId: number,
        public name: string,
        public level: number,
        public race: Race,
        public clazz: Clazz
    ) {}
    
}