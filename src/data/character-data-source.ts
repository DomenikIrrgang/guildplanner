import { Character } from "./character";

export interface CharacterDataSource {
    getAllCharacters(): Character[]
}