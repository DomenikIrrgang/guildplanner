export enum Race {
    HUMAN,
    DWARF,
    GNOME,
    NIGHTELF,
    TAUREN,
    ORC,
    UNDEAD,
    TROLL
}