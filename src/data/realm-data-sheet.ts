import { RealmDataSource } from "./realm-data-source";
import { Realm } from "./realm";
import { Spreadsheet } from "./spreadsheet";

export class RealmDataSheet implements RealmDataSource {

    constructor(private sheetName: string, private spreadsheet: Spreadsheet) {}

    public init(): void {
        this.spreadsheet.createSheet(this.sheetName)
        this.spreadsheet.getSheetbyName(this.sheetName).getRange("A1:B1").setValues([["Id", "Name"]])
        this.save({
            id: 14,
            name: "PTR"
        })
    }

    public getAllRealms(): Realm[] {
        return this.loadAllRealmData()
    }

    public save(data: Realm) {
        if (data.id === undefined) {
            this.spreadsheet.getSheetbyName(this.sheetName).getRange(`A${this.count() + 2}:B${this.count() + 2}`).setValues([[this.count() + 1, data.name]])
        } else {
            this.spreadsheet.getSheetbyName(this.sheetName).getRange(`A${data.id + 1}:B${data.id + 1}`).setValues([[ data.id, data.name]])
        }
    }

    private count(): number {
        let tmp = 1;
        for (let realmData of this.spreadsheet.getSheetbyName(this.sheetName).getRange("A2:E100").getValues()) {
            if (realmData[0] === "") {
                return tmp - 1
            }
            tmp++;
        }
        return tmp
    }

    private getDataRange(): GoogleAppsScript.Spreadsheet.Range {
        return this.spreadsheet.getSheetbyName(this.sheetName).getRange("A2:E100");
    }

    private loadAllRealmData(): Realm[] {
        let realmData = this.spreadsheet.getSheetbyName(this.sheetName).getRange("A2:E100").getValues()
        let result = []
        for (let character of realmData) {
            result.push(this.parseRealmData(character))
        }
        return result
    }

    private parseRealmData(realmData: Object[]): Realm {
        return {
            "id": <number> realmData[0],
            "name": <string> realmData[1]
        }
    }

}