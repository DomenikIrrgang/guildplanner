export class Spreadsheet {

    private spreadSheet: GoogleAppsScript.Spreadsheet.Spreadsheet;

    public constructor() {
        this.spreadSheet = SpreadsheetApp.getActiveSpreadsheet()
    }

    public getData(sheet: string, column: number, row: number): Object {
        return this.getSheetbyName(sheet).getDataRange().getValues()[column][row]
    }

    public getSheetbyName(name: string): GoogleAppsScript.Spreadsheet.Sheet {
        return this.spreadSheet.getSheetByName(name)
    }

    public createSheet(name: string) {
        if (this.getSheetbyName(name) == undefined) {
            this.spreadSheet.insertSheet(name)
        }
    }
}