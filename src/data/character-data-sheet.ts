import { CharacterDataSource } from "./character-data-source";
import { Character } from "./character";
import { Spreadsheet } from "./spreadsheet";
import { Config } from "../config";
import { Race } from "./race";
import { Clazz } from "./class";

export class CharacterDataSheet implements CharacterDataSource {

    constructor(private spreadsheet: Spreadsheet) {}

    public getAllCharacters(): Character[] {
        return this.loadAllCharacterData()
    }

    private loadAllCharacterData(): Character[] {
        let characterData = this.spreadsheet.getSheetbyName(Config.sheets.characters).getRange("A2:F100").getValues()
        let result = []
        for (let character of characterData) {
            result.push(this.parseCharacterData(character))
        }
        return result
    }

    private parseCharacterData(characterData: Object): Character {
        return new Character(
            <number> characterData[0],  
            <number> characterData[1],           
            <string> characterData[2],
            <number> characterData[3],
            <Race> characterData[4],
            <Clazz> characterData[5]
        )
    }

}