import { Realm } from "./realm";

export interface RealmDataSource {
    getAllRealms(): Realm[]
    init(): void
}